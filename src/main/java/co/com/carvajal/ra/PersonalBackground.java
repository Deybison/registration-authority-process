package co.com.carvajal.ra;

import java.util.Arrays;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class PersonalBackground implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private Long idPersonalBackground;
	private Long idServiceRequest;
	private String antecedentType;
	private String antecedentName;
	private String personValid;
	private String description;
	private String backgroundQueryNumber;
	private String observation;
	private byte[] file;
	private String userSession;

	

	public PersonalBackground() {
	}

	public Long getIdPersonalBackground() {
		return idPersonalBackground;
	}

	public void setIdPersonalBackground(Long idPersonalBackground) {
		this.idPersonalBackground = idPersonalBackground;
	}

	public Long getIdServiceRequest() {
		return idServiceRequest;
	}

	public void setIdServiceRequest(Long idServiceRequest) {
		this.idServiceRequest = idServiceRequest;
	}

	public String getAntecedentType() {
		return antecedentType;
	}

	public void setAntecedentType(String antecedentType) {
		this.antecedentType = antecedentType;
	}

	public String getPersonValid() {
		return personValid;
	}

	public void setPersonValid(String personValid) {
		this.personValid = personValid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBackgroundQueryNumber() {
		return backgroundQueryNumber;
	}

	public void setBackgroundQueryNumber(String backgroundQueryNumber) {
		this.backgroundQueryNumber = backgroundQueryNumber;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getUserSession() {
		return userSession;
	}

	public void setUserSession(String userSession) {
		this.userSession = userSession;
	}

	public String toString() {
		return "PersonalBackgroundDTO [idPersonalBackground="
				+ idPersonalBackground + ", idServiceRequest="
				+ idServiceRequest + ", antecedentType=" + antecedentType +", antecedentName=" + antecedentName 
				+ ", personValid=" + personValid + ", description="
				+ description + ", backgroundQueryNumber="
				+ backgroundQueryNumber + ", observation=" + observation
				+ ", file=" + Arrays.toString(file) + ", userSession="
				+ userSession + "]";
	}

	public byte[] getFile() {
		return this.file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public String getAntecedentName() {
		return this.antecedentName;
	}

	public void setAntecedentName(String antecedentName) {
		this.antecedentName = antecedentName;
	}

	public PersonalBackground(java.lang.Long idPersonalBackground,
			java.lang.Long idServiceRequest, java.lang.String antecedentType, java.lang.String antecedentName,
			java.lang.String personValid, java.lang.String description,
			java.lang.String backgroundQueryNumber,
			java.lang.String observation, byte[] file,
			java.lang.String userSession) {
		this.idPersonalBackground = idPersonalBackground;
		this.idServiceRequest = idServiceRequest;
		this.antecedentType = antecedentType;
		this.antecedentName =antecedentName;
		this.personValid = personValid;
		this.description = description;
		this.backgroundQueryNumber = backgroundQueryNumber;
		this.observation = observation;
		this.file = file;
		this.userSession = userSession;
	}

}